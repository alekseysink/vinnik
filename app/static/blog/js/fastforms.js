/**
 * Created by eugene-s on 5/1/15.
 *
 * version: 0.0.3a
 *
 * @todo Update DOCS
 */

(function(factory) {
    "use strict";
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (window.jQuery && !window.jQuery.fn.fastform) {
        factory(window.jQuery);
    }
}
(function($) {
    var defaults = {
        success: function () {},
        data: function () {
            var $inputs = $(this).find("input[name]:not(:has([type=submit])), select, textarea");
            var $other_inputs = $("[form=" + $(this).attr("id") + "]");

            var data = {};

            $inputs.each(function () {
                if (typeof $(this).attr("not-input") === "undefined") {
                    data[$(this).attr("name")] = $(this).val();
                }
            });
            $other_inputs.each(function () {
                if (typeof $(this).attr("not-input") === "undefined") {
                    data[$(this).attr("name")] = $(this).val();
                }
            });

            return data;
        },
        url: function() { return this.url; },
        valueURL: ""
    };

    function FastForms(element, options) {
        this.defaults = defaults;
        this.element = $(element);
        this.options = $.extend({}, this.defaults, this.element.data(), options);

        for (var name in options)
            this.options[name] = options[name];
    }

    FastForms.prototype = {
        constructor: FastForms,
        success: function (option, action_name) {
            var rv;
            var action_name = action_name || "";

            if (typeof option === "function") {
                this.options["success" + action_name] = option;
            } else {
                if (typeof option === "string") {
                    action_name = option;
                    if (this.check("success" + action_name)) {
                        rv = this.options["success" + action_name];
                    } else {
                        rv = this.options["success"];
                    }
                }
            }

            return rv;
        },
        data: function (option, action_name) {
            var rv;
            var action_name = action_name || "";

            if (typeof option === "function") {
                this.options["data" + action_name] = option;
            } else {
                if (typeof option === "string") {
                    action_name = option;
                    if (this.check("data" + action_name)) {
                        rv = this.options["data" + action_name].call(this.element);
                    } else {
                        rv = this.options["data"].call(this.element);
                    }
                }
            }

            return rv;
        },
        url: function(option, action_name) {
            var rv;
            var action_name = action_name || "";

            if (typeof option === "function") {
                this.options["url" + action_name] = option;
            } else {
                if (typeof option === "string") {
                    action_name = option;
                    var $that = {
                        url: this.options.valueURL,
                        form: this.element
                    };
                    if (this.check("url" + action_name)) {
                        rv = this.options["url" + action_name].call($that);
                    } else {
                        rv = this.options["url"].call($that);
                    }
                }
            }

            return rv;
        },
        setURL: function(option) {
            this.options.valueURL = option;
        },
        check: function(option) {
            if (option) {
                return this.options[option] ? true : false;
            } else {
                return true;
            }
        }
    };

    $.fn.fastform = function(option) {
        if ($(this).attr('fastform') !== "undefined") {
            var formArgs = arguments,
                rv;

            var $returnValue = this.each(function () {
                var $this = $(this),
                    inst = $this.data('fastform'),
                    options = ((typeof option === 'object') ? option : {});
                if ((!inst) && (typeof option !== 'string')) {
                    $this.data('fastform', new FastForms(this, options));
                } else {
                    if (!inst) {
                        $this.fastform();
                    } else {
                        if (typeof option === 'string') {
                            rv = inst[option].apply(inst, Array.prototype.slice.call(formArgs, 1));
                        }
                    }
                }
            });
            if (typeof rv) {
                return rv;
            }
            return $returnValue;
        }
    };

    $("form[fastform]").each(function() {
        $(this).fastform();
        $(this).on("submit", function(e) {
            e.preventDefault();

            var $btn = $(document.activeElement);
            var $form = $(this);

            var action_name = (
            $btn.length &&
            $form.has($btn) &&
            $btn.is('[type="submit"]')
            ) ? $btn.attr("action-name") : "";

            var action_url = action_name === "" ? $form.attr("action") : $form.attr("action-" + action_name);
            $form.fastform("setURL", action_url);

            $.ajax({
                url: $form.fastform("url", action_name),
                method: $form.attr("method"),
                data: JSON.stringify($form.fastform("data", action_name)),
                dataType: "JSON",
                contentType: "application/json",
                success: $form.fastform("success", action_name)
            });
        });
    });
}));