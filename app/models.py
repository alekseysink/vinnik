from google.appengine.ext import db


class Admin(db.Model):
    email = db.EmailProperty(required=True)


class BlogArticles(db.Model):
    title = db.StringProperty(required=True)
    body = db.StringProperty(required=True)
    group_article = db.StringProperty(required=True)
    group_photos = db.StringProperty()


class BlogPhotos(db.Model):
    group_photos = db.StringProperty(required=True)
    photo = db.BlobProperty()


class ArticleAboutMe(db.Model):
    body = db.TextProperty()
    photo = db.BlobProperty()


class PhotoAboutMe(db.Model):
    photo = db.BlobProperty()


class Traineeship(db.Model):
    body = db.TextProperty()


class Courses(db.Model):
    body = db.TextProperty()


class Education(db.Model):
    body = db.TextProperty()


class Work(db.Model):
    body = db.TextProperty()


class Project(db.Model):
    body = db.TextProperty()


class MyProject(db.Model):
    body = db.TextProperty()


class Conference(db.Model):
    body = db.TextProperty()


class ContactsEmail(db.Model):
    email = db.EmailProperty(required=True)


class ContatctsSocial(db.Model):
    name = db.StringProperty(required=True)
    url = db.StringProperty(required=True)
    logo = db.BlobProperty()


class AllContacts(db.Model):
    nameColumn = db.StringProperty(required=True)
    valueColumn = db.StringProperty(required=True)


class GroupsOfBlogs(db.Model):
    name = db.StringProperty(required=True)
    color = db.StringProperty()


class Blog(db.Model):
    name = db.StringProperty(required=True)
    content = db.StringProperty(required=True, multiline=True)
    group = db.ReferenceProperty(GroupsOfBlogs, required=True)
    date = db.DateTimeProperty(required=True, auto_now_add=True)


class Font(db.Model):
    name = db.StringProperty(required=True)
    link = db.StringProperty(required=True)
    css = db.StringProperty(required=True)


class Settings(db.Model):
    font = db.ReferenceProperty(Font, required=True)
