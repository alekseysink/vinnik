from flask.ext.wtf import Form
from wtforms import validators, StringField, TextField, TextAreaField


class ArticleAboutForm(Form):
    article_about_me = TextAreaField(u'Article About Me')


class EmailForm(Form):
    email = StringField(u"email", validators=[validators.DataRequired(message=u"Email is required"),
                                              validators.Email(message=u"Invalid email")])


class AdminForm(Form):
    email = StringField(u"email", validators=[validators.DataRequired(message=u"Email is required"),
                                              validators.Email(message=u"Invalid email")])


class FontForm(Form):
    name = StringField(u"NameFont", validators=[validators.DataRequired(message=u"Name is Required")])
    link = StringField(u'LinkFont', validators=[validators.DataRequired(message=u'Url is Required')])
    css = StringField(u'CSSFont', validators=[validators.DataRequired(message=u'Url is Required')])


class ContactSocialForm(Form):
    name = StringField(u"NameSocial", validators=[validators.DataRequired(message=u"Name is Required")])
    url = StringField(u'UrlSocial', validators=[validators.DataRequired(message=u'Url is Required')])


class AllContactForm(Form):
    column = StringField(u"column", validators=[validators.DataRequired(message=u"Column is Required")])
    value = StringField(u'value', validators=[validators.DataRequired(message=u'Value is Required')])