from app import app
from flask import render_template
from models import *

PER_PAGE = 10

@app.route('/')
def index():
    return render_template('index.html', settings=Settings.all().get())


@app.route('/upcoming/')
def upcoming():
    return render_template('upcoming-events.html', settings=Settings.all().get())


@app.route('/blog/', defaults={'page': 1, 'group': 0})
@app.route('/blog/group/<int:group>', defaults={'page': 1})
@app.route('/blog/page/<int:page>', defaults={'group': 0})
@app.route('/blog/group/<int:group>/page/<int:page>')
def blog(group, page):
    from paginator import Paginator, InvalidPage, EmptyPage

    if group:
        model = Blog.all().filter('group = ', GroupsOfBlogs.get_by_id(group)).fetch(100)
    else:
        model = Blog.all().fetch(100)

    paginator = Paginator(model, 3)

    if page:
        try:
            page = int(page)
        except ValueError:
            page = 1
        try:
            paginator = paginator.page(page)
        except (EmptyPage, InvalidPage):
            paginator = paginator.page(paginator.num_pages)

    return render_template('blog.html', group=group, paginator=paginator, current=page, groups=GroupsOfBlogs.all(),
                           settings=Settings.all().get())


@app.route('/contact/')
def contact():
    return render_template('contact.html', emails=ContactsEmail.all(), networks=ContatctsSocial.all(),
                           contacts=AllContacts.all(), settings=Settings.all().get())


@app.route('/work/')
def work():
    article = Work().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('work.html', article=article, settings=Settings.all().get())


@app.route('/projects/')
def projects():
    article = Project().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('projects.html', article=article, settings=Settings.all().get())


@app.route('/my-projects')
def my_projects():
    article = MyProject().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('my-projects.html', article=article, settings=Settings.all().get())


@app.route('/traineeship')
def traineeship():
    article = Traineeship().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('traineeship.html', article=article, settings=Settings.all().get())


@app.route('/courses')
def courses():
    article = Courses().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('courses.html', article=article, settings=Settings.all().get())


@app.route('/conference')
def conference():
    article = Conference().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('conference.html', article=article, settings=Settings.all().get())


@app.route('/education')
def education():
    article = Education().all().get()
    if not article:
        article = ""
    else:
        article = article.body
    return render_template('education.html', article=article, settings=Settings.all().get())


@app.route('/refresh_about_me_img/<string:id>', methods=["GET"])
def refresh_about_me_img(id):
    return app.response_class(PhotoAboutMe.get_by_id(long(id)).photo, mimetype="image/png")


@app.route('/logo/<string:key>', methods=["GET"])
def logo(key):
    return app.response_class(db.get(key).logo, mimetype="image/png")


@app.route('/serve_img/<string:key>', methods=["GET"])
def serve_img(key):
    photo = db.get(key)
    if photo:
        return app.response_class(photo.photo, mimetype="image/png")